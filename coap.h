#include <stdlib.h>
#include <avr/io.h>

#ifndef COAP_H
#define COAP_H


struct HDRStruct{
  unsigned int id :16;
  unsigned int code :8;
  unsigned int opcount :4;
  unsigned int type :2;
  unsigned int ver :2;
};

union HDRType {
  struct HDRStruct pack;
  uint32_t bytes;
  uint8_t block[4];
};

struct OPTStruct{
  uint8_t length :4;
  uint8_t delta :4;
};

union OPTByte{
  struct OPTStruct pack;
  uint8_t byte;
};

struct Option {
  union OPTByte header;
  uint8_t *value;
};

union int_bytes{
    uint32_t val;
    uint8_t block[4];
};

struct Option* make_opt_int(uint8_t delta, uint32_t value);

class Vector{
  private:
    uint8_t *base;
    uint16_t size;
    uint16_t capacity;

  public:
  Vector();
  ~Vector();
  uint8_t*  get_base();
  uint16_t   get_size();
  uint16_t  get_capacity();

  int push_byte (uint8_t data);

};

#endif
