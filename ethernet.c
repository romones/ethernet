#include "ethernet.h"
#include "spi.h"

void ethernet_init(unsigned char mac_addr[], unsigned char ip_addr[],
    unsigned char sub_mask[], unsigned char gtw_addr[]){

  SPI_Write(MR,0b10000000);  
  SPI_Write(TMSR,0x55);
  SPI_Write(RMSR,0x55);
  
    SBASE[0] = TXBUF_BASE + SSIZE * 0;
    RBASE[0] = RXBUF_BASE + RSIZE * 0;
    
    SBASE[1] = TXBUF_BASE + SSIZE * 1;
    RBASE[1] = RXBUF_BASE + RSIZE * 1;

    SBASE[2] = TXBUF_BASE + SSIZE * 2;
    RBASE[2] = RXBUF_BASE + RSIZE * 2;

    SBASE[3] = TXBUF_BASE + SSIZE * 3;
    RBASE[3] = RXBUF_BASE + RSIZE * 3;

  SPI_Write(SIPR + 0,ip_addr[0]);
  SPI_Write(SIPR + 1,ip_addr[1]);
  SPI_Write(SIPR + 2,ip_addr[2]);
  SPI_Write(SIPR + 3,ip_addr[3]);
  
  SPI_Write(SUBR + 0,sub_mask[0]);
  SPI_Write(SUBR + 1,sub_mask[1]);
  SPI_Write(SUBR + 2,sub_mask[2]);
  SPI_Write(SUBR + 3,sub_mask[3]);
  
  SPI_Write(SAR + 0,mac_addr[0]);
  SPI_Write(SAR + 1,mac_addr[1]);
  SPI_Write(SAR + 2,mac_addr[2]);
  SPI_Write(SAR + 3,mac_addr[3]);
  SPI_Write(SAR + 4,mac_addr[4]);
  SPI_Write(SAR + 5,mac_addr[5]);
  
  SPI_Write(GAR + 0,gtw_addr[0]);
  SPI_Write(GAR + 1,gtw_addr[1]);
  SPI_Write(GAR + 2,gtw_addr[2]);
  SPI_Write(GAR + 3,gtw_addr[3]);

  printf("Init Done\n");
  printf("Reading GAR: %d.%d.%d.%d\n\n",SPI_Read(GAR + 0),SPI_Read(GAR + 1), 
      SPI_Read(GAR + 2),SPI_Read(GAR + 3));
  printf("Reading SAR: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n\n",SPI_Read(SAR + 0),SPI_Read(SAR + 1),
      SPI_Read(SAR + 2),SPI_Read(SAR + 3),SPI_Read(SAR + 4),SPI_Read(SAR + 5));
  printf("Reading SUBR: %d.%d.%d.%d\n\n",SPI_Read(SUBR + 0),SPI_Read(SUBR + 1),
      SPI_Read(SUBR + 2),SPI_Read(SUBR + 3));
  printf("Reading SIPR: %d.%d.%d.%d\n\n",SPI_Read(SIPR + 0),SPI_Read(SIPR + 1),
      SPI_Read(SIPR + 2),SPI_Read(SIPR + 3));

}

int socket_set(uint8_t num, sockaddr* addr, uint8_t type){
  
  if (num > 4) return 0;
  
  if (SPI_Read(SR[num]) != SOCK_CLOSED ) return 0;

  SPI_Write(SMR[num], type);
  
  SPI_Write(PORT[num], addr->port[0]); 
  SPI_Write(PORT[num] + 1, addr->port[1]);

  SPI_Write(DIPR[num], addr->ip[0]);
  SPI_Write(DIPR[num] + 1, addr->ip[1]);
  SPI_Write(DIPR[num] + 2, addr->ip[2]);
  SPI_Write(DIPR[num] + 3, addr->ip[3]);


  SPI_Write(DHAR[num], addr->mac[0]);
  SPI_Write(DHAR[num] + 1, addr->mac[1]);
  SPI_Write(DHAR[num] + 2, addr->mac[2]);
  SPI_Write(DHAR[num] + 3, addr->mac[3]);
  SPI_Write(DHAR[num] + 4, addr->mac[4]);
  SPI_Write(DHAR[num] + 5, addr->mac[5]);

  SPI_Write(DPORT[num], addr->dport[0]);
  SPI_Write(DPORT[num] + 1, addr->dport[1]);
  
  SPI_Write(CR[num],0x01); //OPEN
  
  while(SPI_Read(CR[num]));
  printf("Socket %d:\n",num);
  printf("\tStatus: 0x%x\n",SPI_Read(SR[num]));
  printf("\tLocal Port: %d\n", ( (SPI_Read(PORT[num]) & 0x00FF ) << 8 ) + SPI_Read(PORT[num] +1));
  printf("\tDst IP: %d.%d.%d.%d\n",SPI_Read(DIPR[num]),
             SPI_Read(DIPR[num]+1),SPI_Read(DIPR[num]+2),SPI_Read(DIPR[num]+3));
  printf("\tDst Port: %d\n", ( (SPI_Read(DPORT[num]) & 0x00FF ) << 8 ) + SPI_Read(DPORT[num] +1));
  printf("\tDst MAC: %x:%x:%x:%x:%x:%x\n", SPI_Read(DHAR[num]+0), SPI_Read(DHAR[num]+1),
      SPI_Read(DHAR[num]+2),SPI_Read(DHAR[num]+3),SPI_Read(DHAR[num]+4),SPI_Read(DHAR[num]+5));

  return 1;
}

int listen(uint8_t num){
  
  if (SPI_Read(SR[num]) != SOCK_INIT ){
    printf ("Socket %d, not ready for listen\n", num);
    return 0;
  }

  SPI_Write(CR[num],CR_LISTEN);

  while(SPI_Read(CR[num]));

  printf("Socket %d:\n",num);
  printf("\tStatus: 0x%x\n",SPI_Read(SR[num]));

  return 1;

}

int send(uint8_t num, uint8_t *data, uint16_t size){
  
  uint16_t phys, offset, txmax, pos;

  static uint16_t counter = 0;
  txmax = SPI_Read(TX_FSR[num]);
  txmax = (((txmax & 0x00FF) << 8 ) + SPI_Read(TX_FSR[num] + 1));

     if (size > txmax) {
       printf ("Too much to transmit %d, aborting\n",size);
       return 0;
     }
  
  pos = SPI_Read(TX_WR[num]);   
  offset = (((pos & 0x00FF) << 8 ) + SPI_Read(TX_WR[num] + 1));

  
   while(size){
    size--;

    phys = TXBUFADDR + (offset & TX_BUF_MASK);
    SPI_Write(phys, *data);

    offset++;
    data++;
   }

  SPI_Write(TX_WR[num],(offset & 0xFF00) >> 8 );
  SPI_Write(TX_WR[num] + 1,(offset & 0x00FF)); 
          

  printf("[%d] TX_WR: %x\t",counter,offset);
  
  pos = SPI_Read(TX_RR[num]);   
  offset = (((pos & 0x00FF) << 8 ) + SPI_Read(TX_RR[num] + 1));

  printf("TX_RR: %x\n",offset);

  SPI_Write(CR[0],CR_SEND_MAC);

  while(SPI_Read(CR[0])); 
  counter++;
}



