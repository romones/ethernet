#include <avr/io.h>

#ifndef SPI_H
#define SPI_H

#define SPI_PORT PORTB
#define SPI_DDR  DDRB
#define SPI_CS   PORTB4

// Wiznet W5100 Op Code
#define WIZNET_WRITE_OPCODE 0xF0
#define WIZNET_READ_OPCODE 0x0F

int freeRam (); 

void SPI_Write(unsigned int addr,unsigned char data);
unsigned char SPI_Read(unsigned int addr);
void SPI_Init(void);

#endif
