#include <avr/io.h>
#include <string.h>
#include <stdio.h>
#include <util/delay.h>

#ifndef SERIAL_H
#define SERIAL_H

#define BAUD_RATE 9600

void uart_init(void);
int uart_putch(char ch,FILE *stream);
int uart_getch(FILE *stream);


#endif



