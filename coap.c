#include "coap.h"

struct Option* make_opt_int(uint8_t delta,uint32_t value){
   
 if (delta > 16) return NULL;
     
  union int_bytes tmp;
  tmp.val = value;
 
  int length = 0;
  int i;
   for (i = 3;i>=0;i--){ 
     if (tmp.block[i] != 0) {
        length = i+1;
        break;
     }
   }
 
 uint8_t *arr;  
 arr = (uint8_t*) malloc (sizeof(uint8_t) * length);    
  for (i = 0; i < length; i++){
   arr[i] = tmp.block[i];
  }
 
 struct Option* retval = (struct Option *) malloc (sizeof(uint8_t) * (length + 1));

 retval->header.pack.delta = delta;
 retval->header.pack.length = length;
 retval->value = arr;

 return retval;

}     

Vector::Vector(){
  base = NULL;
  size = 0;
  capacity = 0;
}

Vector::~Vector(){
  if (base != NULL) free (base);
}

uint8_t* Vector::get_base(){
    return base;
}

uint16_t Vector::get_size(){
    return size;
}

uint16_t Vector::get_capacity(){
    return capacity;
}

int Vector::push_byte(uint8_t data){
 
  if (base == NULL){
    base = (uint8_t*) malloc (sizeof(uint8_t) * 4);
    if (base == NULL) return 0;
    
    capacity = 4;
  }
  
  if (size == capacity) {
    uint8_t *tmp = (uint8_t *)realloc(base, capacity * 2);
    if (tmp == NULL)  return 0;
    
    base = tmp;
    capacity *= 2;
  }
  
  base[size] = data;
  size++;
  return 1;
 
} 

