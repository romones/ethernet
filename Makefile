CC = avr-gcc
CPP = avr-g++
BOARD = -mmcu=atmega2560
CLK = -DF_CPU=16000000UL

hex: obj
	avr-objcopy -O ihex -R .eeprom wiznet wiznet.hex

obj: main.o serial.o spi.o ethernet.o coap.o
	$(CC) $(BOARD) main.o serial.o spi.o ethernet.o coap.o -o wiznet

main.o: main.c
	$(CPP) -Os $(CLK) $(BOARD) -c main.c -o main.o

serial.o: serial.c serial.h
	$(CPP) -Os $(CLK) $(BOARD) -c serial.c -o serial.o

spi.o: spi.c spi.h
	$(CPP) -Os $(CLK) $(BOARD) -c spi.c -o spi.o

ethernet.o: ethernet.c ethernet.h
	$(CPP) -Os $(CLK) $(BOARD) -c ethernet.c -o ethernet.o

coap.o: coap.c coap.h
	$(CPP) -Os $(CLK) $(BOARD) -c coap.c -o coap.o

upload: hex
	avrdude -F -V -c arduino -p m2560 -c avrispmkII  \
	-P /dev/ttyACM0  -U flash:w:wiznet.hex

clean:
	rm -vf *.o
	rm -vf wiznet wiznet.hex
	
