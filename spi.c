#include "spi.h"

int freeRam () {
    extern int __heap_start, *__brkval; 
      int v; 
        return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

void SPI_Write(unsigned int addr,unsigned char data)
{
  // Activate the CS pin
  SPI_PORT &= ~(1<<SPI_CS);

  // Start Wiznet W5100 Write OpCode transmission
  SPDR = WIZNET_WRITE_OPCODE;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));

  // Start Wiznet W5100 Address High Bytes transmission
  SPDR = (addr & 0xFF00) >> 8;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));

  // Start Wiznet W5100 Address Low Bytes transmission
  SPDR = addr & 0x00FF;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));   

  // Start Data transmission
  SPDR = data;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));

  // CS pin is not active
  SPI_PORT |= (1<<SPI_CS);
}

unsigned char SPI_Read(unsigned int addr)
{
  // Activate the CS pin
  SPI_PORT &= ~(1<<SPI_CS);

  // Start Wiznet W5100 Read OpCode transmission
  SPDR = WIZNET_READ_OPCODE;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));

  // Start Wiznet W5100 Address High Bytes transmission
  SPDR = (addr & 0xFF00) >> 8;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));

  // Start Wiznet W5100 Address Low Bytes transmission
  SPDR = addr & 0x00FF;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));   

  // Send Dummy transmission for reading the data
  SPDR = 0x00;

  // Wait for transmission complete
  while(!(SPSR & (1<<SPIF)));  

  // CS pin is not active
  SPI_PORT |= (1<<SPI_CS);

  return(SPDR);
}

void SPI_Init(void){
  SPI_DDR = (1<<PORTB2)|(1<<PORTB1)|(1<<PORTB0);

  // CS pin is not active
  SPI_PORT |= (1<<SPI_CS);

  // Enable SPI, Master Mode 0, set the clock rate fck/2
  SPCR = (1<<SPE)|(1<<MSTR);
  SPSR |= (1<<SPI2X);
  DDRB  |=  _BV(4);
}
