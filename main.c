#include <avr/io.h>
#include <string.h>
#include <stdio.h>
#include <util/delay.h>

#include "serial.h"
#include "spi.h"
#include "ethernet.h"
#include "coap.h"

 unsigned char mac_addr[] = {0x00,0x08,0xDC,0xDE,0x58,0xF7};
 unsigned char ip_addr[] = {192,168,88,10};
 unsigned char sub_mask[] = {255,255,255,0};
 unsigned char gtw_addr[] = {192,168,88,1};

 unsigned char dst_addr[] = {239,1,2,3};
 unsigned char dst_port[] = {0x16,0x33}; // dec 5683 - CoAP port
 unsigned char src_port[] = {0x16,0x00};
 unsigned char dst_mac[] = {0x01,0x00,0x5E,0x01,0x02,0x03};

 unsigned char blank_addr[] = {0,0,0,0};
 unsigned char blank_port[] = {0x00,0x00};
 unsigned char tcp_port[] = {0x00,0x50};
 unsigned char blank_mac[] = {0x00,0x00,0x00,0x00,0x00,0x00};


FILE *uart_str = fdevopen(uart_putch, uart_getch); 

int main(){

  stdout = stdin = uart_str;
  uart_init();
	
  SPI_Init();
 
  ethernet_init(mac_addr, ip_addr, sub_mask, gtw_addr);
 
  sockaddr dst;
  memcpy (dst.mac, dst_mac, 6);
  memcpy (dst.ip, dst_addr, 4);
  memcpy (dst.port, src_port, 2);
  memcpy (dst.dport, dst_port, 2);

  
  sockaddr tcp;
  memcpy (tcp.mac, blank_mac, 6);
  memcpy (tcp.ip, blank_addr, 4);
  memcpy (tcp.port, tcp_port, 2);
  memcpy (tcp.dport, blank_port, 2);
  
  socket_set(0,&dst,0b10000010);
  socket_set(1,&tcp,0b00000001);
  listen (1);

//Socket handling
 
 uint16_t ptr,offaddr,realaddr,txsize;
 uint8_t *msg;
 uint16_t buflen;
 int i; 


 union HDRType hdr;
 hdr.pack.ver = 1;
 hdr.pack.id = 0xBEEF;
 hdr.pack.code = 2;
 hdr.pack.opcount = 2;
 hdr.pack.type = 3;


 Vector frame;
 
 for (i=3;i>=0;i--){
   frame.push_byte(hdr.block[i]);
 }

 uint8_t type = 0x10;
 uint16_t max_age = 285;
 
 int last_opt = 0;


 struct Option* opt = make_opt_int(1, (uint32_t)type);
 printf ("Option is: %d of %d bytes long \n", *(opt->value),opt->header.pack.length);

 frame.push_byte(opt->header.byte);
  for (i=opt->header.pack.length;i>0;i--){
    frame.push_byte(*(opt->value + i-1));
    printf("\n%d\n",*(opt->value));
  }
 
 
 struct Option* opt2 = make_opt_int(1, (uint32_t)max_age); 
 frame.push_byte(opt2->header.byte);
  for (i=opt2->header.pack.length;i>0;i--){
    frame.push_byte(*(opt2->value + i-1));
  }

 msg = frame.get_base(); 
  
  while (1){
  printf("Free RAM:%d\n", freeRam());
  send(0,msg, frame.get_size());
  _delay_ms(5000);
 }

return 0;
}
